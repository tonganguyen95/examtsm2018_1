#PART 3

import numpy as np
import pandas as pd
import os
import random
from random import randint
from random import choices

#I call the class "Customer from the module "Part2"

from Code.Part2  import Customer

#I import all the necessary datafarames
#These 3 are the same as in PART 2
NewDataframe = pd.read_csv("../Results/NewDataframe.csv", sep=',')
DrinkProbaDataframe = pd.read_csv("../Results/DrinkProbaDataframe.csv", sep=',')
FoodProbaDataframe = pd.read_csv("../Results/FoodProbaDataframe.csv", sep=',')

#I want to create a dataframe that contains the same information about the bar (copy of "NewDataframe") but I want to display the Day and Time in 2 separate columns
#For this, I split the column "TIME" in 2 columns named "Date" and "Time"
#for "Date",we will have the following format: "Year-Month-Day"
#for "Time", we will have the following format: "Hour:Minute"
#I call my new dataframe : "InitialDataNewTimeFormat"
InitialDataNewTimeFormat = NewDataframe.copy(deep=True)
InitialDataNewTimeFormat[['Date','Time']]= InitialDataNewTimeFormat.TIME.str.split(expand=True)
print(InitialDataNewTimeFormat)

#I save it
path = "../Results/InitialDataNewTimeFormat.csv"
InitialDataNewTimeFormat.to_csv(os.path.join(path))


### THE SIMULATION #####

#I create empty lists that I will use to store element of each column of the dataframe from the simulation
S_Time=[]
S_CustomerID =[]
S_Drinks =[]
S_Food =[]
S_Price = []
S_Date = []

for row in InitialDataNewTimeFormat.itertuples(index=False, name=None): #I iterate the following procedure over rows of the dataframe "InitialDataNewTimeFormat"
        S_Date.append(row[5])
        S_Time.append(row[6])
        Frequency=' '
        Type=' '
        Random=choices(['OneTimeCustomer','ReturningCustomer'],[0.80,0.2])# randomly generate a Frequency for the customer using their (numeric) distribution i.e: probability to be a One time customer is 0.8
        if Random==['OneTimeCustomer']: #If it a one time customer
           Frequency='OneTimeCustomer'
           TypeName = choices(['FromTripAdvisor','Regular'],[0.1,0.9]) #Randomly generate the Frequency of the customer.The proba to randomly generate a customer from TriAdvisor is 0.1 as they represent a tenth of the one time customers.
           if TypeName==['FromTripAdvisor']:
               Type= 'FromTripAdvisor' #if the
           else:# print('OTRegular')
               Type='Regular'
           OneTimeCustomerIDNumber = str(row[0])
           ID = str('OT'+OneTimeCustomerIDNumber)#Chose an ID which is a randomNumber.All the OneTime customer have an ID OF the following format: "OTrow_index" t
           CustomerCreation = Customer(ID,Frequency,Type,100) #Create a One Time customer. The attribute Budget will be automatically set up to 100
           Drink = CustomerCreation.DrinkPurchased(row[6]) #generate a random drink
           Food = CustomerCreation.FoodPurchased(row[6])  #generate a random food
           TotalPrice = CustomerCreation.TotalAmountPaid(Drink,Food) #compute the total amount
        else: # if it is a Returning customer
           Frequency='ReturningCustomer'
           TypeName = choices(['Hipster', 'Regular'], [0.33, 0.67])
           if TypeName== ['Hipster']:
                Type= 'Hipster'
                Budget =500 #if it is a Hipster the attribute  Budget will be automatically 500
           else:  # if it is a
                Type='Regular'
                Budget=250 #if it is a Regular the attribute  Budget will be automatically 250
           ReturningCustomerNumber = list(range(1000))  # All the returning customer have an ID of the format "RET + an IDNumber in this list"
           ProbabilityList = [0.001] * 1000  # When a Returning customer is drawn, the proba. of occurrence of each element in this list "ReturningCustomerIDNumber" is the same = 0.001
           ReturningCustomerIDNumber = str(choices(ReturningCustomerNumber, ProbabilityList))
           ID = str('RET' + ReturningCustomerIDNumber)  # All the returning customer have an ID of the following format "RET[ReturningCustomerID]"
           CustomerCreation = Customer(ID, Frequency, Type, Budget)  # Create a Returning customer
           Drink = CustomerCreation.DrinkPurchased(row[6])
           Food = CustomerCreation.FoodPurchased(row[6])
           TotalPrice = CustomerCreation.TotalAmountPaid(Drink,Food)
        S_CustomerID.append(CustomerCreation.ID) #Add the customer attribute ID to the list "S_CustomerID"
        S_Drinks.append(Drink) #Add the drink generated to the list "S_Drink"
        S_Food.append(Food) #Add the food generated to the list "S_Food"
        S_Price.append(TotalPrice) #Add the TotalPrice generated to the list "S_Price"
#Once the loop has finished to iterate the procedure over all the rows of the the dataframe "InitialDataNewTimeFormat", we use the list that hae been filled up
#by the IDs,Drinks,Foods & TotalPrices to create the column of the dataframe from the simulation
SimulationDataFrame = pd.DataFrame({'S_DATE':S_Date,'S_TIME': S_Time, 'S_CUSTOMER': S_CustomerID, 'S_DRINKS': S_Drinks, 'S_FOOD': S_Food,'S_TotalPrice': S_Price}, index=list(range(len(S_CustomerID))))
print(SimulationDataFrame)


#Save it
path1 = "../Results/SimulationDataFrame.csv"
SimulationDataFrame.to_csv(os.path.join(path1))


SimulationDataframe = pd.read_csv("../Results/SimulationDataframe.csv", sep=',')

#Plot the average cash flow during the day
##The simulation dataset span 5 years.
##I'm going to generate a series of days ans their corresponding Total Cash Flow
# sum up all the total prices paid by customers at a given date, to get the Cash flow of this day
##For this, I use the function "groupby"  & I take the sum of elemnts in the column "S_TotalPrice"
AverageCashFlowOfDay = SimulationDataframe.groupby('S_DATE')['S_TotalPrice'].sum()
AverageCashFlowOfDay=AverageCashFlowOfDay.reset_index()
AverageCashFlowOfDay.rename(columns={'S_TotalPrice':'CashFlow'}, inplace=True) #The column 'S_TotalPrice' will contain the average cash flow of the day.Thus,I rename it
# print(AverageCashFlowOfDay)

path2 = "../Results/AverageCashFlowOfDay.csv"
AverageCashFlowOfDay.to_csv(os.path.join(path2))


import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.ticker import FormatStrFormatter

#read data from "AverageCashFlowOfDay.csv"
data=pd.read_csv("../Results/AverageCashFlowOfDay.csv", sep=',',usecols=['S_DATE','CashFlow'],parse_dates=['S_DATE'])
#set date as index
data.set_index('S_DATE', inplace=True)
#plot data
fig, ax = plt.subplots(figsize=(15,7))
data.plot(ax=ax)
#set ticks every year
ax.xaxis.set_major_locator(mdates.YearLocator())
#set major ticks format (Month-Year)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%b %Y'))
# display the plot
plt.show()
#save the figure
fig.savefig(os.path.abspath("../Results/PlotAverageCashFlowOfDay.png"))



#COMPARISON OF THE DATAFRAME FROM THE SIMULATION WITH THE ORIGINAL ONE

#For the original one, let's see the number of occurences of each ID
NumberOfOccurences = NewDataframe.CUSTOMER.value_counts()
# print(NumberOfOccurences)
#The one time customers are thos for whch the ID appears only once in all the dataset
#We create a list of them from the dataset and print the length of the list
#The length of the list is the number of One Time customers
ListOfOneTimeCustomers = NumberOfOccurences[NumberOfOccurences== 1].index.tolist()
print(len(ListOfOneTimeCustomers))   ## There are 246988 One Time Customers => 1000 returning customers in the initial Dataframe

#For the simulation one, let's see the number of occurences of each ID
NumberOfOccurences1 = SimulationDataframe.S_CUSTOMER.value_counts()
print(NumberOfOccurences1)
#The one time customers are thos for whch the ID appears only once in all the dataset
#We create a list of them from the dataset and print the length of the list
#The length of the list is the number of One Time customers
ListOfOneTimeCustomers1 = NumberOfOccurences1[NumberOfOccurences1 == 1].index.tolist()
print(len(ListOfOneTimeCustomers1))  ## There are 249680 One Time Customers => 1000 returning customers in the simulation Dataframe

#==>The proportions of One Time & returning Customers are almost the same.