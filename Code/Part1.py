import numpy as np
import pandas as pd
import os

#Let's import our original Coffee bar dataframe
df = pd.read_csv("../Data/Coffeebar_2013-2017.csv", sep=';')
#I made a copy of it that I will throughout all my code(to avoid altering the original dataframe)
InitialDataframe = df.copy(deep=True)
path0 = "../Results/InitialDataframe.csv"
InitialDataframe.to_csv(os.path.join(path0))

# print(InitialDataframe)

#PART 1

# 1.What food are sold by the coffee bar?
Foodlist=list(InitialDataframe['FOOD'].dropna().unique())
Foodlist= ",".join(Foodlist)
print('The food sold are: %s' %str(Foodlist)) #sandwich,pie,muffin,cookie
# 2.What drinks are sold by the coffee bar?
Drinklist=list(InitialDataframe['DRINKS'].dropna().unique())
Drinklist= ", ".join(Drinklist)
print('The drinks sold are: %s' %str(Drinklist)) #frappucino,soda,coffee,tea, water, milkshake
# 3. How many unique customers
print('The number of unique customers is: %s' %str(len(InitialDataframe['CUSTOMER'].dropna().unique()))) # number => 247988



# 4. BAR PLOTTING

#Convert the time column  of our initial dataframe to type Datetime
import datetime as dt

InitialDataframe['TIME'] = pd.to_datetime(InitialDataframe.TIME)
path1 = "../Results/InitialDataframe.csv"
InitialDataframe.to_csv(os.path.join(path1))

# 4.1. Bar Plot of the total amount of drinks

#I create an object of type pandas.core.series that contains quarterly Sales of Drinks
DrinkSalesByQuarter = InitialDataframe.set_index('TIME').resample('3M')["DRINKS"].count() #pbpython.com/pandas-grouper-agg.html
print(DrinkSalesByQuarter)
import matplotlib.pyplot as plot1

fig, ax1 = plot1.subplots(figsize=(10,5))
DrinkSalesByQuarter.plot(ax=ax1,kind='bar',stacked=True, color="b")
ax1.xaxis.set_ticklabels(DrinkSalesByQuarter.index.to_period('3M'))
ax1.set_xlabel('Date')
ax1.set_ylabel('Total Amount')
ax1.set_title('Quarterly Sales of Drinks')
plot1.show()
fig.savefig(os.path.abspath("../Results/plot1.png"))

# 4.2. Bar Plot of the total amount of Food
#I create an object of type pandas.core.series that contains quarterly Sales of Food
FoodSalesByQuarter = InitialDataframe.set_index('TIME').resample('3M')["FOOD"].count() #pbpython.com/pandas-grouper-agg.html
# print(FoodSalesByQuarter)
import matplotlib.pyplot as plot2

#Create a plot with color blue('b')
fig, ax2 = plot2.subplots(figsize=(20,10))
FoodSalesByQuarter.plot(ax=ax2,kind='bar',stacked=True, color="b")
ax2.xaxis.set_ticklabels(FoodSalesByQuarter.index.to_period('3M'))
#specify the x-axis label
ax2.set_xlabel('Date')
#specify the y-axis label
ax2.set_ylabel('Total Amount')
#Add a title
ax2.set_title('Quarterly Sales of Food')
#Display the plot
plot2.show()
#Save the figure
fig.savefig(os.path.abspath("../Results/plot2.png"))

# # 5. PROBABILITY HAT A CUSTOMER BUYS A CERTAIN FOOD OR DRINK A ANY GIVE TIME

#I will use a copy of my initial dataframe(new dataframe called "NewDataframe")
NewDataframe= InitialDataframe.copy(deep=True)
#replace all the missing values by the value 'NoFood'
NewDataframe.fillna('NoFood',inplace=True)
#save the dataframe
path2 = "../Results/NewDataframe.csv"
NewDataframe.to_csv(os.path.join(path2))
# print(NewDataframe)



# #Tip: count number of customers for each itemm at any given time
# # Some sources:
# # http://pbpython.com/pandas-grouper-agg.html
# #resample('60Min')
#
# 5.1. FOR DRINKS:
#I create a copy of the dataframe "NewDataframe"
NewDataframeforDrinks = NewDataframe.copy(deep=True)
# In this copy, I change the format of time to the format "Hour:Minute"
NewDataframeforDrinks['TIME'] = pd.to_datetime(NewDataframeforDrinks.TIME).dt.strftime('%H:%M') #strftime.org/
#I delete the column 'FOOD' as it will not be useful for this part
del NewDataframeforDrinks['FOOD']
# print(NewDataframeforDrinks)

#I create a dataframe named "NumberOfDrinksSoldOnEachTime" which will be a pivot table that contains numbers of customers for each time and for each drink
# #I count the number of Customers for each type of drink by Time.
NumberOfDrinksSoldOnEachTime = NewDataframeforDrinks.groupby(['TIME','DRINKS']).count().reset_index()
print(NumberOfDrinksSoldOnEachTime)
# # pivot table
NumberOfDrinksSoldOnEachTime=NumberOfDrinksSoldOnEachTime.pivot(index='TIME', columns='DRINKS', values='CUSTOMER')
# print(NumberOfDrinksSoldOnEachTime)
#replace missing vlues by 0
NumberOfDrinksSoldOnEachTime.fillna(0, inplace=True)
# print(NumberOfDrinksSoldOnEachTime)

#The table with probability of a customer to buy a drink at any given time is obtained  by dividing each cOlumn by the sum of the columns
#I will call it "DrinkProbaDataframe"
DrinkProbaDataframe = NumberOfDrinksSoldOnEachTime.div(NumberOfDrinksSoldOnEachTime.sum(axis=1), axis=0)
print(DrinkProbaDataframe)

#save the dataframe
path3 = "../Results/DrinkProbaDataframe.csv"
DrinkProbaDataframe.to_csv(os.path.join(path3))



# 5.1. FOR FOOD (same procedure):
#I create a copy of the dataframe "NewDataframe"
NewDataframeforFood = NewDataframe.copy(deep=True)
# In this copy, I change the format of time to the format "Hour:Minute"
NewDataframeforFood['TIME'] = pd.to_datetime(NewDataframeforFood.TIME).dt.strftime('%H:%M') #strftime.org/
#I delete the column 'FOOD' as it will not be useful for this part
del NewDataframeforFood['DRINKS']
# print(NewDataframeforFood)

#I create a dataframe named "NumberOfFoodSoldOnEachTime" which will be a pivot table that contains numbers of customers for each time nd for each type of food
# #I count the number of Customers for each type of drink by Time.
# Temp = NewDataframeforFood[NewDataframeforFood['FOOD']== "sandwich"] #premier sandwich à 11:00, dernier à 12:58
# print(Temp)
NumberOfFoodSoldOnEachTime = NewDataframeforFood.groupby(['TIME','FOOD']).count().reset_index()
# print(NumberOfFoodSoldOnEachTime)

# # pivot table
NumberOfFoodSoldOnEachTime=NumberOfFoodSoldOnEachTime.pivot(index='TIME', columns='FOOD', values='CUSTOMER')
#replace missing values by 0
NumberOfFoodSoldOnEachTime.fillna(0, inplace=True)
# print(NumberOfFoodSoldOnEachTime)

#The table with probability of a customer to buy a drink at any given time is obtained  by dividing each column by the sum of the columns
#I will call it "FoodProbaDataframe"
FoodProbaDataframe = NumberOfFoodSoldOnEachTime.div(NumberOfFoodSoldOnEachTime.sum(axis=1), axis=0)
print(FoodProbaDataframe)

path4 = "../Results/FoodProbaDataframe.csv"
FoodProbaDataframe.to_csv(os.path.join(path4))